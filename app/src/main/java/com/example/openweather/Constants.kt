package com.example.openweather

// Constant values used through out the application
object Constants {
    const val baseURl: String = "https://api.openweathermap.org"
    const val imageURl: String = "https://openweathermap.org"
    const val socketTimeout: Long = 60
    const val apiKey: String = "appid"
    const val apiValue: String = "b629fc52afd5e05d6e1fab9f4a1d170a"
}