package com.example.weatherapplication

import android.app.Application
import com.example.openweather.BaseApplication
import dagger.hilt.android.HiltAndroidApp
/*

// Need to enable this for using hilt in unit testing

@HiltAndroidApp
class WeatherApplication: BaseApplication(){
}

 */